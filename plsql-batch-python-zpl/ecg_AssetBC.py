import sys
import socket
import math
import time
import logging

image = """eJzt07ENwyAQBVAQhUtG8CiMBlKKlBkho8SjeARLaSgsLhx3kG8lrtKGwnpCzoH8f4z5cUWivZuIitJWk9qxk3hiL2LPXsUzexMHdh7jxwG8F4qO3/g3On7lWUnGL/rQLScHtFetHCAj5IC4t+u1A+ozPulR2puFfSe5Pfsm0yhVX9Vr9UW9VSd1Bhcwoam7oAN4BnuwA1uwOdmfTubg/Pj9PnRwiZokfzJwBm/gBZzAptvyN+9NqYnE3qDYEgn7O6+YR45GcvQyYR25O8m9RT5JH1rknt79mctnr7Bv2EPsJ/YW+4w9x/4f/hf/dbpeLRp10g==:D9E7"""
waitTime = 5

# Command Line : python c:\BC_FILES\ecg_ItemBC.py <PRN_IPADDR> <TAG_AMOUNT> <START_TAG> <NUM_COPIES>

logger = logging.getLogger('ecg_AssetBC')
hdlr = logging.FileHandler('C:\BC_FILES\ecg_AssetBC.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

def prnAsset(ipAddr,bcAmt,bcStart,bcCopy):
  TCP_IP = ipAddr
  TCP_PORT = 9100
  BUFFER_SIZE = 1024

  maxTagAmt = 5 # 25
  bcRemaining = bcAmt
  tagAmt = min(maxTagAmt,bcRemaining)
  tagStart = bcStart
  tagVal = bcStart
  loopAmt = math.ceil(bcAmt/maxTagAmt)

  loop1 = 0
  loop2 = 0

  for idx in range (0,int(loopAmt)):
    tagAmt = min(maxTagAmt,bcRemaining)
    loop2 = 0

    for x in range (0,tagAmt):
      loop2 = loop2 + 1
      tagVal = tagStart+loop1
      #this will print a code 128 barcode
      zpl = """
      ^XA
      ^PW600
      ^FO448,0^GFA,03456,03456,00012,:Z64:"""+image+"""
      ^BY3,3,116^FT441,81^BCI,,Y,N
      ^FD>;"""+str(tagVal)+"""^FS
      ^FT53,292^BQN,2,3
      ^FH\^FDLA,www.tribal-knowledge.co^FS
      ^FT448,232^A0I,50,50^FH\^FDManaged Asset^FS
      ^PQ"""+str(bcCopy)+""",0,1,Y^XZ
      """

      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.connect((TCP_IP, TCP_PORT))
      #s.send(bytes(zpl, "utf-8"))
      s.send(str(zpl))
      s.close()
      loop1 = loop1 + 1
      bcRemaining = bcRemaining - 1

    time.sleep(waitTime)

if __name__ == "__main__":
    a = sys.argv[1]
    b = int(sys.argv[2])
    c = int(sys.argv[3])
    d = int(sys.argv[4])
    try:
        prnAsset(a, b, c, d)
        logger.info('[RUN] '+ a +' : '+ str(b) +' : '+ str(c) +' : '+ str(d))
    except Exception as e:
        logging.error('[RUN] '+ a +' : '+ str(b) +' : '+ str(c) +' : '+ str(d))
