import sys
import socket
import logging

image = """eJzt07ENwyAQBVAQhUtG8CiMBlKKlBkho8SjeARLaSgsLhx3kG8lrtKGwnpCzoH8f4z5cUWivZuIitJWk9qxk3hiL2LPXsUzexMHdh7jxwG8F4qO3/g3On7lWUnGL/rQLScHtFetHCAj5IC4t+u1A+ozPulR2puFfSe5Pfsm0yhVX9Vr9UW9VSd1Bhcwoam7oAN4BnuwA1uwOdmfTubg/Pj9PnRwiZokfzJwBm/gBZzAptvyN+9NqYnE3qDYEgn7O6+YR45GcvQyYR25O8m9RT5JH1rknt79mctnr7Bv2EPsJ/YW+4w9x/4f/hf/dbpeLRp10g==:D9E7"""

# Command Line : python c:\BC_FILES\ecg_ItemBC.py <ITEMNUM> <AMOUNT> <PRN_IPADDR>

logger = logging.getLogger('ecg_ItemBC')
hdlr = logging.FileHandler('C:\BC_FILES\ecg_ItemBC.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

def prnItem(ItemNum,Amt,IPAddr):
    TCP_IP = IPAddr
    TCP_PORT = 9100
    BUFFER_SIZE = 1024

    #this will print a code 128 barcode
    zpl = """
    ^XA
    ^PW600
    ^FO448,0^GFA,03456,03456,00012,:Z64:"""+image+"""
    ^BY3,3,116^FT441,81^BCI,,Y,N
    ^FD"""+ItemNum+"""^FS
    ^FT53,292^BQN,2,3
    ^FH\^FDLA,www.tribal-knowledge.co^FS
    ^FT448,232^A0I,50,50^FH\^FDInventory Item^FS
    ^PQ"""+str(Amt)+""",0,1,Y^XZ
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    #s.send(bytes(zpl, "utf-8"))
    s.send(str(zpl))
    s.close()

if __name__ == "__main__":
    a = sys.argv[1]
    b = int(sys.argv[2])
    c = sys.argv[3]
    try:
        prnItem(a, b, c)
        logger.info('[RUN] '+ a +' : '+ str(b) +' : '+ c)
    except Exception as e:
        logger.error('[RUN] '+ a +' : '+ str(b) +' : '+ c)
