--------------------------------------------------------
--  File created - Wednesday-May-09-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence BCS_MAIN_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BCS_MAIN_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table BCS_BARCODE_INDEX
--------------------------------------------------------

  CREATE TABLE "BCS_BARCODE_INDEX" 
   (	"ID_RECORD" NUMBER, 
	"DESCRIPTION" VARCHAR2(100), 
	"BARCODE_INDEX" NUMBER, 
	"DATE_LAST_RUN" DATE, 
	"BC_TYPE" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table BCS_BCIDX_HIST
--------------------------------------------------------

  CREATE TABLE "BCS_BCIDX_HIST" 
   (	"ID_RECORD" NUMBER, 
	"ID_BCIDX" NUMBER, 
	"BC_START" NUMBER, 
	"BC_AMT" NUMBER, 
	"PRINT_IP" VARCHAR2(50), 
	"DATE_RUN" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table BCS_PRN_REG
--------------------------------------------------------

  CREATE TABLE "BCS_PRN_REG" 
   (	"ID_RECORD" NUMBER, 
	"DESCRIPTION" VARCHAR2(100), 
	"PRN_LOC" VARCHAR2(50), 
	"PRINT_IP" VARCHAR2(50)
   ) ;
REM INSERTING into BCS_BARCODE_INDEX
SET DEFINE OFF;
Insert into BCS_BARCODE_INDEX (ID_RECORD,DESCRIPTION,BARCODE_INDEX,DATE_LAST_RUN,BC_TYPE) values (2,'Maximo Inventory Tags',null,to_date('08-MAY-18','DD-MON-RR'),'SINGLE');
Insert into BCS_BARCODE_INDEX (ID_RECORD,DESCRIPTION,BARCODE_INDEX,DATE_LAST_RUN,BC_TYPE) values (1,'Maximo Asset Tags',2201,to_date('08-MAY-18','DD-MON-RR'),'SERIES');
REM INSERTING into BCS_BCIDX_HIST
SET DEFINE OFF;
REM INSERTING into BCS_PRN_REG
SET DEFINE OFF;
Insert into BCS_PRN_REG (ID_RECORD,DESCRIPTION,PRN_LOC,PRINT_IP) values (1,'Barcode Test Printer','HQ Jason''s Desk','192.168.16.216');
--------------------------------------------------------
--  DDL for Package PKG_BCPRINT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PKG_BCPRINT" as 

procedure p_itembc
(p_itemnum in varchar2  -- item number for the tag(s)
,p_amt in number        -- number of copies of the item tag
,p_ipaddr in varchar2); -- ip address of the zebra printer

procedure p_assetbc_bulk
(p_ipaddr in varchar2 -- ip address of the zebra printer
,p_bcamt in number    -- number of tags in the print run
,p_bcstart in number  -- starting value for print run
,p_bccopy in number); -- number of copies of each tag
  
end pkg_bcprint;

/
--------------------------------------------------------
--  DDL for Package Body PKG_BCPRINT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PKG_BCPRINT" as

procedure p_itembc
(p_itemnum in varchar2  -- item number for the tag(s)
,p_amt in number        -- number of copies of the item tag
,p_ipaddr in varchar2) 
as
  vname varchar(20) := 'ITEMBC';
BEGIN
  begin dbms_scheduler.drop_job(vname); exception when others then null; end ;
  dbms_scheduler.create_job(
    job_name => vname,
    job_type => 'EXECUTABLE',
    job_action => 'C:\Windows\System32\cmd.exe',
    number_of_arguments => 2,
    enabled => FALSE);
  dbms_scheduler.set_job_argument_value( job_name => vname, argument_position => 1, argument_value => '/c');
  dbms_scheduler.set_job_argument_value( job_name => vname, argument_position => 2, argument_value => 'c:\BC_FILES\runItemBC.cmd '||p_itemnum||' '||p_amt||' '||p_ipaddr);
  dbms_scheduler.enable(vname);

end p_itembc;

procedure p_assetbc_bulk
(p_ipaddr in varchar2 -- ip address of the zebra printer
,p_bcamt in number    -- number of tags in the print run
,p_bcstart in number  -- starting value for print run
,p_bccopy in number) 
as
  vname varchar(20) := 'ASSETBC';
BEGIN
  begin dbms_scheduler.drop_job(vname); exception when others then null; end ;
  dbms_scheduler.create_job(
    job_name => vname,
    job_type => 'EXECUTABLE',
    job_action => 'C:\Windows\System32\cmd.exe',
    number_of_arguments => 2,
    enabled => FALSE);
  dbms_scheduler.set_job_argument_value( job_name => vname, argument_position => 1, argument_value => '/c');
  dbms_scheduler.set_job_argument_value( job_name => vname, argument_position => 2, argument_value => 'c:\BC_FILES\runAssetBC.cmd '||p_ipaddr||' '||p_bcamt||' '||p_bcstart||' '||p_bccopy);
  dbms_scheduler.enable(vname);

end p_assetbc_bulk;

end pkg_bcprint;

/
